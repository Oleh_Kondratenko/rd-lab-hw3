const express = require('express');
const {
  getUserTrucks,
  addTruck,
  getUserTruckById,
  patchTruck,
  removeTruck,
  assignTruckToUser,
} = require('../services/truckService');
const {asyncWrapper} = require('../utils/apiUtils');
const router = new express.Router();


router.get('/', asyncWrapper(async (req, res, next) => {
  const {_id} = req.user;
  const trucks = await getUserTrucks(_id);
  res.status(200).json({trucks: [...trucks], message: 'success'});
}));
router.post('/', asyncWrapper(async (req, res, next) => {
  const {_id} = req.user;
  const {type} = req.body;
  await addTruck(_id, type);
  res.status(200).json({message: 'Truck has been added'});
}));

router.get('/:id', asyncWrapper(async (req, res, next) => {
  const truckId = req.params.id;
  const userId = req.user._id;
  const truck = await getUserTruckById({truckId, userId});
  res.status(200).json({
    message: 'success',
    truck: {
      '_id': truck._id,
      'created_by': truck.created_by,
      'assigned_to': truck.assigned_to,
      'type': truck.type,
      'status': truck.status,
      'created_date': truck.created_date,
    },
  });
}));

router.put('/:id', asyncWrapper(async (req, res, next) => {
  const truckId = req.params.id;
  const userId = req.user._id;
  const {type: truckType} = req.body;
  await patchTruck({truckId, userId, truckType});
  res.status(200).json({message: 'Updated successfully'});
}));

router.delete('/:id', asyncWrapper(async (req, res, next) => {
  const truckId = req.params.id;
  const userId = req.user._id;
  await removeTruck({truckId, userId});
  res.status(200).json({message: 'Your truck has been deleted'});
}));

router.post('/:id/assign', asyncWrapper(async (req, res, next) => {
  const truckId = req.params.id;
  const userId = req.user._id;
  console.log('truck id asssign asdasd asd as da', truckId, userId);
  await assignTruckToUser({truckId, userId});
  res.status(200).json({message: 'The truck has been assigned to you'});
}));


module.exports = {
  trucksRouter: router,
};