const express = require('express');
const router = new express.Router();
const {
  asyncWrapper,
} = require('../utils/apiUtils');
const {
  createLoadForShipper,
  getUserLoads,
  getActiveLoad,
  iteratateLoadState,
  getUserLoadById,
  changeUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  findTruckForTransportation,
  getLoadShippingInfoById,
} = require('../services/loadService');


router.get('/', asyncWrapper(async (req, res, next) => {
  const {_id} = req.user;
  const {status, limit = 10, offset = 0} = req.query;
  const loads = await getUserLoads({_id, limit, offset, status});
  res.status(200).json({
    message: 'Success',
    loads: loads,
  });
}));

router.post('/', asyncWrapper(async (req, res, next) => {
  const {_id: created_by} = req.user;
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    width,
    length,
    height,
  } = req.body;
  await createLoadForShipper({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    width,
    length,
    height,
    created_by,
  });
  res.status(200).json({message: 'load has been added'});
}));


router.get('/active', asyncWrapper(async (req, res, next) => {
  const {_id, role} = req.user;
  const load = await getActiveLoad({_id, role});
  res.status('200').json({message: 'success', load: load});
}));

router.patch('/active/state', asyncWrapper(async (req, res, next) => {
  const {_id, role} = req.user;
  const load = await iteratateLoadState({_id, role});
  res.status('200').json({message: `load state changed to ${load.state}`});
}));

router.get('/:id', asyncWrapper(async (req, res, next) => {
  const {_id: userId, role} = req.user;
  const {id: loadId} = req.params;
  const load = await getUserLoadById({userId, role, loadId});
  res.status(200).json({load: load});
}));

router.put('/:id', asyncWrapper(async (req, res, next) => {
  const loadSetting = req.body;
  const {id: loadId} = req.params;
  const {_id: userId, role} = req.user;
  await changeUserLoadById(loadSetting, {loadId, userId, role});
  res.status(200).json({message: 'Load details changed successfully'});
}));

router.delete('/:id', asyncWrapper(async (req, res, next) => {
  const {id: loadId} = req.params;
  const {_id: userId, role} = req.user;
  await deleteUserLoadById({loadId, userId, role});
  res.status(200).json({message: 'Load was deleted'});
}));

router.post('/:id/post', asyncWrapper(async (req, res, next) => {
  const {id: loadId} = req.params;
  const {_id: userId, role} = req.user;
  const load = await postUserLoadById({loadId, userId, role});
  let truck = await findTruckForTransportation(load);
  /*   while (!truck) {
      truck = await findTruckForTransportation(load);
    } */
  res.status(200).json({
    message: 'Load posted successfully', driver_found: !!truck,
  });
}));

router.get('/:id/shipping_info', asyncWrapper(async (req, res, next) => {
  const {id: loadId} = req.params;
  const {_id: userId, role} = req.user;
  const {load, truck} = await getLoadShippingInfoById({loadId, userId, role});
  res.status(200).json({load: load, truck: truck});
}));

module.exports = {
  loadsRouter: router,
};