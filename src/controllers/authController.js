const express = require('express');
const router = new express.Router();
const {
  registration,
  signIn,
} = require('../services/authService');
const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.post('/register', asyncWrapper(async (req, res, next) => {
  const {
    email: username,
    password,
    role,
  } = req.body;
  try {
    await registration({
      username,
      password,
      role,
    });
  } catch (error) {
    const err = new Error(error.message);
    err.status = 400;
    throw err;
  }
  res.status(200).json({
    message: 'Successfully registered',
  });
}));

router.post('/login', asyncWrapper(async (req, res, next) => {
  const {
    email: username,
    password,
  } = req.body;
  const token = await signIn({
    username,
    password,
  });
  res.status(200).json({
    jwt_token: token,
    message: 'Successfully signed in',
  });
}));


module.exports = {
  authRouter: router,
};