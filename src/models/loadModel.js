const mongoose = require('mongoose');

const logSchema = new mongoose.Schema({
  message: {
    type: String,
    default: 'no description',
  },
  time: {
    type: Date,
    default: Date.now(),
  },
}, {_id: false});

const dimensionsSchema = new mongoose.Schema({
  width: {
    type: Number,
    required: true,
  },
  height: {
    type: Number,
    required: true,
  },
  length: {
    type: Number,
    required: true,
  },
}, {_id: false});

const loadShcema = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  logs: {
    type: [logSchema],
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
    ref: 'Users',
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    required: true,
    default: 'NEW',
    set: function (str) {
      return str.toUpperCase();
    },
  },
  state: {
    type: String,
    enum: ['En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery',
    ],
  },
  name: {
    type: String,
  },
  dimensions: {
    type: dimensionsSchema,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
    min: 0,
    max: 4000,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

const LoadModel = mongoose.model('Loads', loadShcema);

module.exports = {
  LoadModel,
  dimensionsSchema,
};