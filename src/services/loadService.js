const {LoadModel} = require('../models/loadModel');
const {UserModel} = require('../models/userModel');
const {TruckModel} = require('../models/truckModel');

const createLoadForShipper = async ({
  name,
  payload,
  pickup_address,
  delivery_address,
  dimensions,
  width,
  length,
  height,
  created_by,
}) => {
  const user = await UserModel.findOne({_id: created_by});
  const userRole = String(user.role).toUpperCase();
  if (userRole !== 'SHIPPER') {
    const err = new Error('only shippers can create loads');
    err.status = 403;
    throw err;
  }
  const load = new LoadModel({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    width,
    length,
    height,
    created_by,
  });

  user.loads.push(load._id);
  await user.save();
  await load.save();
};

const getUserLoads = async ({_id, limit, offset, status}) => {
  limit = Number(limit) >= 10 ? 10 : Number(limit);
  offset = Number(offset);
  const user = await UserModel.findOne({_id: _id});
  const role = String(user.role).toUpperCase();
  const statusQuery = status ? {status: status} : {};

  if (role === 'DRIVER') {
    const loads = await LoadModel.find({assigned_to: _id}, ...statusQuery);
    return loads;
  } else if (role === 'SHIPPER') {
    const loads = await LoadModel.find(
      {'_id': {$in: user.loads}, ...statusQuery},
    ).skip(offset).limit(limit);
    return loads;
  } else {
    const err = new Error('Server error');
    err.status = 500;
    throw err;
  }
};

const getActiveLoad = async ({_id, role}) => {
  role = String(role).toUpperCase();
  if (role !== 'DRIVER') {
    const err = new Error('Only for drivers');
    err.status = 403;
    throw err;
  }
  const user = await UserModel.findOne({_id: _id});

  const load = await LoadModel.findOne({
    assigned_to: user._id, status: 'ASSIGNED',
  });
  return load;
};

const iteratateLoadState = async ({_id, role}) => {
  role = String(role).toUpperCase();
  if (role !== 'DRIVER') {
    const err = new Error('Only for drivers');
    err.status = 403;
    throw err;
  }

  const load = await getActiveLoad({_id, role});
  const user = await UserModel.findOne({_id: load.assigned_to});
  const truck = await TruckModel.findOne({
    _id: user.assignedTruck,
  });

  console.log(load)
  console.log(truck)

  const loadState = load.state;
  const stateEnums = load.schema.path('state').enumValues;
  let currIdx = stateEnums.findIndex((state) => {
    return loadState === state;
  });

  if (currIdx >= 3) {
    const err = new Error('load has already been delivered');
    err.status = 400;
    throw err;
  }

  load.state = stateEnums[++currIdx];

  if (currIdx === 3) {
    load.status = 'SHIPPED';
    truck.status = 'IS';
  }

  const message = `Load state changed to "${load.state}"`;
  load.logs.push({message: message});
  await load.save();
  await truck.save();
  return load;
};

const getUserLoadById = async ({userId, role, loadId}) => {
  const query = role === 'driver' ? {assigned_to: userId} :
    {created_by: userId};
  const load = await LoadModel.findOne({_id: loadId, ...query});
  if (!load) {
    const err = new Error('wrong load Id');
    err.status = 400;
    throw err;
  }
  return load;
};
const changeUserLoadById = async (loadSettings, {loadId, userId, role}) => {
  role = String(role).toUpperCase();
  if (role !== 'SHIPPER') {
    const err = new Error('Only for shippers');
    err.status = 403;
    throw err;
  }
  const load = await LoadModel.findOne({_id: loadId, created_by: `${userId}`});
  if (!load) {
    const err = new Error('Load with this ID is not yours or does not exist');
    err.status = 403;
    throw err;
  }
  for (const key in loadSettings) {
    if (loadSettings.hasOwnProperty(key)) {
      load[key] = loadSettings[key];
    }
  }
  await load.save();
};

const deleteUserLoadById = async ({loadId, userId, role}) => {
  role = String(role).toUpperCase();
  if (role !== 'SHIPPER') {
    const err = new Error('only for shipper');
    err.status = 403;
    throw err;
  }
  await LoadModel.findOneAndDelete({_id: loadId, created_by: userId});
};

const postUserLoadById = async ({loadId, userId, role}) => {
  role = String(role).toUpperCase();
  if (role !== 'SHIPPER') {
    const err = new Error('only for shipper');
    err.status = 403;
    throw err;
  }
  const load = await LoadModel.findOne({
    _id: loadId,
    created_by: userId,
    status: 'new',
  });
  if (!load) {
    throw Error('no such load');
  }
  load.status = 'posted';
  await load.save();
  return load;
};


const findTruckForTransportation = async (load) => {
  const trucks = await TruckModel.find({
    status: 'IS',
    assigned_to: {$ne: null},
  });

  if (!trucks) {
    return false;
  }
  const {width: widthL, height: heightL, length: lengthL} = load.dimensions;
  const loadDimens = [widthL, heightL, lengthL].sort((curr, next) => {
    return curr - next;
  });
  const loadPayload = load.payload;

  const truck = trucks.find((trk) => {
    const {width: widthT, height: heightT, length: lengthT} = trk.parameters;
    const truckDimens = [widthT, heightT, lengthT].sort((curr, next) => {
      return curr - next;
    });
    const canBeContained = truckDimens.every((side, i, arr) => {
      const truckSide = truckDimens[i];
      const loadSide = loadDimens[i];
      return truckSide >= loadSide;
    });
    const canBeLifted = trk.payload >= loadPayload;
    return canBeContained && canBeLifted;
  });

  if (!truck) {
    return false;
  }

  const user = await UserModel.findOne({_id: truck.assigned_to});

  load.assigned_to = user._id;
  load.status = 'ASSIGNED';
  load.state = load.schema.path('state').enumValues[0];
  load.logs.push({message: `Load assigned to driver with id ${user._id}`});
  truck.status = 'OL';
  user.assignedLoads.push(load._id);
  user.assignedTruck = truck._id;

  await load.save();
  await truck.save();
  await user.save();
  return truck;
};

const getLoadShippingInfoById = async ({loadId, userId, role}) => {
  role = String(role).toUpperCase();
  if (role !== 'SHIPPER') {
    const err = new Error('only for shippers');
    err.status = 403;
    throw err;
  }
  const user = UserModel.findOne({_id: userId});
  const load = LoadModel.findOne({_id: loadId});
  const truck = TruckModel.findOne({_id: user.assignedTruck});
  return {load, truck};
};


module.exports = {
  createLoadForShipper,
  getUserLoads,
  getActiveLoad,
  iteratateLoadState,
  getUserLoadById,
  changeUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  findTruckForTransportation,
  getLoadShippingInfoById,
};