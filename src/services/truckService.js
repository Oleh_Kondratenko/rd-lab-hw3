const {TruckModel} = require('../models/truckModel');
const {UserModel} = require('../models/userModel');

const getUserTrucks = async (userId) => {
  const user = await UserModel.findOne({_id: userId});
  const trucksIds = user.trucks;
  if (trucksIds.length === 0) {
    const err = new Error('You dont have cars yet');
    err.status = 400;
    throw err;
  }
  const trucks = await TruckModel.find(
    {'_id': {$in: trucksIds}},
  );
  return trucks.map((truck) => {
    return {
      '_id': truck._id,
      'created_by': truck.created_by,
      'assigned_to': truck.assigned_to,
      'type': truck.type,
      'status': truck.status,
      'created_date': truck.created_date,
    };
  });
};

const addTruck = async (userId, truckType) => {
  const truck = new TruckModel({
    type: truckType.toUpperCase(),
    created_by: userId,
  });
  await truck.save();
  const user = await UserModel.findOneAndUpdate({_id: userId},
    {$push: {trucks: truck._id}});
  await user.save();
};

const getUserTruckById = async ({truckId, userId}) => {
  const truck = await TruckModel.findOne({_id: truckId, created_by: userId});
  if (!truck) {
    const err = new Error('You have no such truck');
    err.status = 400;
    throw err;
  }
};

const patchTruck = async ({truckId, userId, truckType}) => {
  const truck = await getUserTruckById({truckId, userId});
  truck.type = truckType.toUpperCase();
  await truck.save();
};

const removeTruck = async ({truckId, userId}) => {
  const truck = await TruckModel.findOne({_id: truckId});
  const user = await UserModel.findOne({_id: userId});
  if (String(truck.created_by) === String(user._id)) {
    await TruckModel.findByIdAndRemove({_id: truckId});
    user.trucks = user.trucks.filter((id) => {
      if (String(id) === String(truckId)) {
        return false;
      }
      return true;
    });
    await user.save();
  } else {
    const err = new Error('U cant modify this truck');
    err.status = 400;
    throw err;
  }
};

const assignTruckToUser = async ({truckId, userId}) => {
  const user = await UserModel.findOne({_id: userId});
  const currTruck = await TruckModel.findOne({_id: user.assignedTruck});

  if (currTruck && String(currTruck.status).toUpperCase() === 'OL') {
    const err = new Error('U cant reassing truck while ON LOAD');
    err.status = 400;
    throw err;
  }

  const truck = await TruckModel.findOne({_id: truckId});
  user.assignedTruck = truck._id;
  truck.assigned_to = userId;
  await truck.save();
  await user.save();
};
module.exports = {
  getUserTrucks,
  addTruck,
  getUserTruckById,
  patchTruck,
  removeTruck,
  assignTruckToUser,
};