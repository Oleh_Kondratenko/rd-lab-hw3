const express = require('express');
const morgan = require('morgan');
const app = express();
const {AuthMiddleware} = require('./middlewares/AuthMiddleware');
const {driverOnlyMiddleware} = require('./middlewares/driverOnlyMiddleware');
const mongoose = require('mongoose');
const {authRouter} = require('./controllers/authController');
const {usersRouter} = require('./controllers/usersController');
const {trucksRouter} = require('./controllers/trucksController');
const {loadsRouter} = require('./controllers/loadsController');

const PORT = 8080;

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', [AuthMiddleware], usersRouter);
app.use('/api/trucks', [AuthMiddleware, driverOnlyMiddleware], trucksRouter);
app.use('/api/loads', [AuthMiddleware], loadsRouter);

app.use((err, req, res, next) => {
  if (!err.message) {
    switch (err.status) {
      case 403:
        err.message = 'U have no access to this page';
        break;
      case 401:
        err.message = 'Please login to take access';
        break;
      case 400:
        err.message = 'Something went wrong';
        break;
      default:
        break;
    }
  };
  res.status(err.status || 500).json({
    'message': err.message || 'server error',
  });
});

const startApp = async () => {
  try {
    await mongoose.set('useCreateIndex', true);
    await mongoose.connect('mongodb+srv://Oleh:12345@cluster0.1pnci.mongodb.net/HW3', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    app.listen(PORT, () => {
      console.log(`Example app listening at http://localhost:${PORT}`);
    });
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};
startApp();