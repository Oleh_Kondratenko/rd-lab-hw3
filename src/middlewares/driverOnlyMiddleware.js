const driverOnlyMiddleware = (req, res, next) => {
  const {role} = req.user;
  if (role.toUpperCase() === 'DRIVER') {
    next();
  } else {
    const err = new Error('This page is available only for drivers');
    err.status = 403;
    throw err;
  }
};
module.exports = {
  driverOnlyMiddleware,
};